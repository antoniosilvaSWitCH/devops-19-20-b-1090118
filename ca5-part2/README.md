# Class Assignment 5 - Part 2

## Goals

The goal of this assignment is to crate a pipeline in Jenkins using the project ca2-part2.

## Steps
### Getting Started
The first step was to download and run Jenkins via the WAR file following the instructions of:

    https://www.jenkins.io/doc/book/installing/#war-file

Once Jenkins is executed is installed and a Jenkins account is configured, we are now able to our pipeline.

### Configure pipeline with manual script

In order to create a new pipeline, from the Jenkins main page we select ***New Item***, after which we select
***Pipeline***. After naming this pipeline (in my case called `ca5-part2-manual`), we can configure it by defining a
`Pipeline script` that includes:

    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'antoniosilva', url: 'https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd ca2-part2-v2/demo & gradlew clean assemble'
                }
            }
            stage ('Test'){
                steps {
                 echo 'Testing...'
                 bat 'cd ca2-part2-v2/demo & gradlew test'
                 junit 'ca2-part2-v2/demo/build/test-results/test/*.xml'
                }
            }
            stage('Javadoc'){
                steps {
                    echo 'Publishing...'
                    bat 'cd ca2-part2-v2/demo & gradlew javadoc'
    
                     publishHTML([
                                 reportName: 'Javadoc',
                                 reportDir: 'ca2-part2-v2/demo/build/docs/javadoc/',
                                 reportFiles: 'index.html',
                                 keepAll: false,
                                 alwaysLinkToLastBuild: false,
                                 allowMissing: false
                                 ])
                 }
            }
            stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'ca2-part2-v2/demo/build/libs/*'
                }
            }
            
            stage ('Publish Image') {
                  steps {
                      echo 'Publishing...'
                       script {
                       docker.withRegistry('https://index.docker.io/v1/', 'docker-hub-credentials') {
                                              def customImage =
                                              docker.build("ajms/devops-19-20-b-1090118:ca5-part2-${env.BUILD_ID}", "ca5-part2")
                                              customImage.push()
                                          }
                           }
                      }
                  }
    
        }
    }

This script is intended to link to by BitBucket repository, namely to the ca2-part2-v2 project, with stages for `Checkout`, 
`Assemble`, `Test`, `Javadoc`, `Archive` and `Publish image`. For this last stage the credentials for DockerHub were
included in Jenkins. In order to perform the last stage, a Dockerfile was included in ca5-part2, whose contents are
identical to the `web` Dockerfile from ca4. This stage uploads the image to DockerHub with a prefix `ca5-part2` joined
together with the ID of the build. The contents of the Dockerfile are:

    FROM tomcat
    
    RUN apt-get update -y
    
    RUN apt-get install -f
    
    RUN apt-get install git -y
    
    RUN apt-get install nodejs -y
    
    RUN apt-get install npm -y
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git
    
    WORKDIR /tmp/build/devops-19-20-b-1090118/ca2-part2-v2/demo
    
    RUN chmod u+x gradlew
    
    RUN ./gradlew clean build
    
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
    
    EXPOSE 8080

Once the Pipeline is created, we can perform `Build Now` to run the pipeline. If everything works out, all stages run
successfully.

### Configure pipeline with remote script

Alternatively to the approach referred before, one may also configure the pipeline with a Jenkins script file hosted
remotely. After creating a new Pipeline (named `ca5-part2-remote` in my case), we now select the option
`Pipeline script from SCM` using `Git`. We can then link to `Repository URL` using the saved credentials:
    
    https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git
    
The relevant Jenkins file, which includes script reported above, was hosted in my repository in the folder named
ca5-part1. As such the Pipeline must be configured by assigning to `Script Path` the `ca5-part2/Jenkinsfile` path. 
Once the Pipeline is created, we can perform `Build Now` to run the pipeline. If everything works out, all stages run
successfully.

## Alternative

### Buddy versus Jenkins
As an alternative to Jenkins, Buddy offers some improvements within the CI/CD spectrum both options belong to. This,
however, comes at the detriment of other aspects, which elevate the suitability of Jenkins. These differences, which
were collected from [here](https://hackernoon.com/buddy-vs-jenkins-mt4v32u2), can be summarized as follows:

| Category      | Jenkins | Buddy | 
| :----: | :----: | :----: |
| Installation  | Java application requires installation | No installation needed  	|
| Maintenance  	| Requires regular patching, tuning and cleaning | No maintenance needed  	|
| Integrations  | Open source plugins | Pre-made and tested integrations |
| Pipelines  	| Scripted and UI driven | Scripted and UI driven 	|

Additionally, this [link](https://www.slant.co/versus/537/2477/~buddy_vs_jenkins) provides a community-based comparison
of the two tools, which I have taken the liberty to include below:

Buddy:

* PRO Easy Pipeline Setups: The ease to setup custom pipelines are amazing, can easily various settings quickly and then be ready to deploy.
* PRO Multitude of Actions: Almost any action you can need and think of is already here, making it easy for you to setup your pipeline.
* PRO Nice material design: The design is minimalistic and based on today's standards on material design. It uses colors which are pleasing to the eye and displays the information in an ordered way. The main view shows the latest activity sorted in a chronological order, displaying commits and pushes. Every repo has it's own view, on the top there's the repo's name and a dropdown which displays the current branch with the ability to change to another branch or to create a new one. On the right there's a vertical menu with links to add a new file, show the history or to download the current repository.
* PRO Free private repositories: Private repositories are free. Although they are free for up to 3 repos and each repository must be less than 100MB in size.
* PRO Lots of integrations, for example discord, slack
* CON Unlimited private repositories are not free. To have more than three repositories and to bypass the limit of 100MB per repository it's not free. It costs $3/month.

Jenkins:

* PRO Free and open source: Jenkins is a free and open source continuous integration tool, while its source code is hosted on GitHub.
* PRO Safe to store key environment variables: Self-hosting provides a safe location to store key environment variables since it is the user who is in charge of the server and environment where Jenkins is hosted.
* PRO Highly customizable: Even though Jenkins is pretty functional and useful out of the box, there's a large plugin ecosystem from which the user can choose plugins to integrate into their Jenkins build. This is needed for when the user wants to extend any of the tool's features.
* PRO A lot of resources and tutorials available: Jenkins has been in development since 2004 and is one of the most popular tools of its kind. This means that its technology is very mature and there is a lot of documentation and resources available for it.
* PRO Multiple version control systems supported: Supports the most popular version control systems out of the box: SVN, Mercurial, and Git.
* PRO Scalable: The distributed builds in Jenkins work effectively, thanks to the Master and Slave capabilities.
* PRO Stable release line for users who want less changes: This is called the Jenkins Long-Term Support (LTS) version and helps to provide the most stable and assuring version of the Jenkins CI possible. Every 3 months, a version (which has been deemed the most reliable by the community) is chosen. After this, its branched, well-tested features are added (if they are missing), it is tested with the new features, bug fixes are then carried out if necessary, and from there it is released as the official Jenkins LTS version.
* PRO User can source control their chain of automation: Starting with Jenkins 2.0, the pipeline capability, which has been available as a plugin before this version, has been built into Jenkins itself. This allows developers to describe their chain of automation in text form, which can be version controlled and put alongside the source tree.
* PRO Quantity of available Plugins: For most operations we need not reinvent the wheel, there are plugins already existing.
* PRO Cross-platform build support: Being a Java application it can be installed under any OS: Windows, Linux, and macOS. On the other hand, JNLP slaves also enriches the cross-platform build support for its agents.
* PRO Easy to get up and running: A Jenkins install is very simple and the user can have the service up and running within minutes. To install Jenkins, the command java -jar jenkins.war is all that is needed - nothing more.
* PRO Supports most of the technological stacks for free by specific plugins, including, Docker, Amazon EC2 and S3.
* PRO Great community: Jenkins has a large and helpful community, which welcomes new users and provides a great number of tutorials.
* PRO Self hosted: You stay in full control of your source code, build environment and deployment. No third party gets access to your source code or knows exactly how to build your software.
* PRO Encryption of secrets: Thanks to JENKINS Credentials and Plugin.
* PRO Multiple test environments for different runtime versions: They can be added easily under your Global Configuration.
* PRO Awards and recognition: Including InfoWorld Bossie Award (Best of Open Source Software Award) in 2011, and Received Geek Choice Award in 2014.
* Poor quality plug-ins that are difficult to combine: There have been several complaints by users regarding the quality of the plug-ins found in Jenkins' official plugin repo. A lot of plugins found in the default plugin directory are no longer actively maintained and as a result, they may be incompatible with later versions of Jenkins or other plugins.
* CON High overhead: Unlike some of the simple and hosted alternatives, users need to host and setup Jenkins by themselves. This results in both a high initial setup time, as well as time sunk into maintenance over a project's duration.
* CON Unstable and lack of plugin integration QA process: Jenkins without plugins is almost useless. All plugins are treated equal and published almost right away. Because there is no process for testing Jenkins' integration, the overall Jenkins experience is not that great. Furthermore, Jenkins' core and plugins are released on a regular basis, all requiring instant restarts, meaning that updates appear more than once a day!
* CON Restarting of pipeline steps only available in commercial version: Reliable pipelines with step restarts are only available in the enterprise version. Last time I talked to them in 2018, I was quoted $20k/year for that.
* CON Vulnerable: Vulnerable to cross site and DOS attacks, read article Top 10 Java Vulnerabilities And How To Fix Them.
* CON Limited pipeline size: Pipeline-as-code is limited to a JVM method size limit.

### Implementation
The approach I followed to implement the pipeline in Buddy follows closely the approach by my colleague Rui Ribeiro,
whose contribution is greatly appreciated.

The first step was to create a Buddy account, linking it to my BitBucket account in order to access the repository,
after which a new pipeline (termed ca5-part2) was created.

To replicate the `Assemble` stage from Jenkins, a new action of type `BUILD TOOLS & TASK RUNNERS` and subtype `GRADLE`
was added. In the script part we include:

    cd ca2-part2-v2/demo
    gradle clean assemble

To replicate the `Test` stage, we add a new subsequent action of the same type/subtype with the script:

    cd ca2-part2-v2/demo
    gradle test 

The aforementioned script lacks the `junit` publishing of th test results, whose equivalence in Buddy was not found.
  
To replicate the `Javadoc` stage, we add a new subsequent action of the same type/subtype with the script: 

    cd ca2-part2-v2/demo
    gradle javadoc   

Regarding the `Archive` stage, I did not manage to find how to replicate it in Buddy.

Regarding the `Publish` stage, two steps were defined in Buddy. The first, of type `Build Docker image` , was configured
to link to the Dockerfile in ca5-part2. The second step, of type `Push Docker image`, was configured to link to my
DockerHub account and repository, terming the pushed imaged as ca5-part2-buddy.

After all these stages/steps are configured in Buddy, we can run the pipeline. Luckily, everything worked out
smoothly, and all stages run with success. Unfortunately, and as mentioned before, I was not able to find a complete
alternative for the `Test` stage. Nevertheless, I must recognize that Buddy is incredibly intuitive and easy to use, in
my opinion more so than Jenkins. Even the visual layout Buddy and its stage sequence is more appealing. Admittedly, I
was using the default skin of Jenkins, and so perhaps there are better options to use. 

In order to better evaluate the options I took in this alternative, I have exported the Buddy pipeline to a `*.yml`,
whose contents can be viewed below:

    - pipeline: "ca5-part2"
      trigger_mode: "MANUAL"
      ref_name: "master"
      ref_type: "BRANCH"
      trigger_condition: "ALWAYS"
      actions:
      - action: "Execute: gradle clean assemble"
        type: "BUILD"
        working_directory: "/buddy/devops-19-20-b-1090118"
        docker_image_name: "library/gradle"
        docker_image_tag: "5.4-jdk8"
        execute_commands:
        - "cd ca2-part2-v2/demo"
        - "gradle clean assemble"
        volume_mappings:
        - "/:/buddy/devops-19-20-b-1090118"
        trigger_condition: "ALWAYS"
        shell: "BASH"
      - action: "Execute: junit 'ca2-part2-v2/demo/build/test-results/test/*.xml'"
        type: "BUILD"
        working_directory: "/buddy/devops-19-20-b-1090118"
        docker_image_name: "library/gradle"
        docker_image_tag: "5.4-jdk8"
        execute_commands:
        - "cd ca2-part2-v2/demo"
        - "gradle test"
        volume_mappings:
        - "/:/buddy/devops-19-20-b-1090118"
        trigger_condition: "ALWAYS"
        shell: "BASH"
      - action: "Execute: gradle javadoc"
        type: "BUILD"
        working_directory: "/buddy/devops-19-20-b-1090118"
        docker_image_name: "library/gradle"
        docker_image_tag: "5.4-jdk8"
        execute_commands:
        - "cd ca2-part2-v2/demo"
        - "gradle javadoc"
        volume_mappings:
        - "/:/buddy/devops-19-20-b-1090118"
        trigger_condition: "ALWAYS"
        shell: "BASH"
      - action: "Build Docker image"
        type: "DOCKERFILE"
        dockerfile_path: "ca5-part2/Dockerfile"
        trigger_condition: "ALWAYS"
      - action: "Push Docker image"
        type: "DOCKER_PUSH"
        login: "ajms"
        password: "secure!+toQfVH9RfjnBg+3dV3IiQ==.fwvEQ8XXbEjoSvCsLuGJHg=="
        docker_image_tag: "ca5-part2-buddy"
        repository: "ajms/devops-19-20-b-1090118"
        trigger_condition: "ALWAYS"


## Closure
At the end of the assignment, we update the repository with the last version of this README file and tag the
repository with:

    git commit -a -m "Added final version of ca5-part2 README"
    git push
    git tag ca5-part2
    git push origin ca5-part2
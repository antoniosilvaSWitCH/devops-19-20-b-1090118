# Class Assignment 2 - Part 1

## Goals and requirements

**Goals:**
* Practice Gradle using a very simple example

**Requirements:**
1. Download and commit the example application to personal repository.
2. Read the instructions available in the readme.md file and experiment with the application.
3. Add a new task to execute the server.
4. Add a simple unit test and update the gradle script so that it is able to execute the test.
5. Add a new task of type Copy to be used to make a backup of the sources of the application.
6. Add a new task of type Zip to be used to make an archive of the sources of the application.

## Analysis, design and implementation of the requirements
### Getting started
This assignment was based on a Gradle basic demo, so the first files to be uploaded to the repository were from the demo.
After downloading the file, all the files were uploaded to the repository with a commit:

    git commit -A -m "Adicionados ficheiros base de ca2-part1."
    git push
    
Secondly, and after constructing the initial draft of the present README.md, this file was also added to (i.e. updated in)
the repository.

    git commit -A -m "Added preliminary version of README.md"
    git push

### Build
The next step was to generate a `.jar` file of the project with Gradle, running the following command:

    gradlew build

This command added a build folder to the project with all the project's files: classes, distributions, resources,
scripts, temporary files and also generated a zipped file (`.jar`) in **_build/libs_**  with all this information.

### Run the server
To run the server, the following command was executed:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

Next, in a separate terminal, to run the client we run:

    gradlew runClient

After the successful build message, a chat window is opened. 

### Creating a new task
The next task encompasses the creation to execute the server. So, in the **_build.gradle_** file, a task similar to the
`runClient` task but referring to the `ChatServerApp` class:

    task executeServer(type: JavaExec, dependsOn: classes) {
        
        description = "Executes chat server on localhost:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatServerApp'
    
        args '59001'
    }

In order to check if everything is perfect after adding the aforementioned code, two alternatives may be used, namely:
* Listing the available tasks with: `gradlew tasks --all`
* Running the server, by executing the task: `gradlew executeServer`

It is important to note that the syntax of `executeServer` is slightly different than the existing `runClient` task,
namely regarding the input for the _args_ keyword. Whereas `runClient` needs the host and the port
(e.g. _args 'localhost', '59001'_),`executeServer` needs only the port (e.g. _args '59001'_).
This was not clear at first (i.e. the wrong syntax was firstly used), and an error message highlighting this mistake
appeared in the terminal when trying to execute the task. Luckily, the error message shown is self-explanatory, and the
debugging process was facilitated to the point where the problem was fixed promptly.

### Add unit tests and update Gradle script
In order to add add a unit test to the application, the creation of a test class in _src/test/java/basic_demo/AppTest_.
In this test class, the code given in the class slides was added.

Because the unit tests require junit 4.12 to execute, the dependencies of the project had to be updated to be compatible
with this version:

    dependencies {
        // Use Apache Log4J for logging
        implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        testCompile group: 'junit', name: 'junit', version: '4.12'
    }

After running the unit test with success, these changes have been sent to the repository.

### Copy Task
To create a backup of the sources of the application a `Copy` task has been added to _build.gradle_:

    task backup (type: Copy){
           from 'src'
           into 'backup'
         }

This task copies all files in _src_ to a folder named _backup_.

### Zip Task
A new task of type `Zip` has been added to _build.gradle_ to copy the contents of the _src_ folder to a new `.zip`
contained inside a folder zipfiles:

    task zipFile (type:Zip) {
        from 'src'
        archiveFileName = 'src.zip'
        destinationDirectory = file('zipfiles')
    }
    
### Closure
All the changes of the process described above were committed to the repository using Git. In this context, a tag was
given to mark the assignment, and the files were committed and pushed to the repository: 

    git commit -A -m "Added Copy and Zip tasks. Added final version of README.md"
    git push

    git tag ca2-part1
    git push origin ca2-part1

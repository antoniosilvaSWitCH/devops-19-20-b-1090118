# Class Assignment 3 - Part 2

## Goals
Use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle "basic" version
(developed in ca2-part2)

## Steps
### Getting Started
The first step was to create a subfolder for part2 of CA3 and then commit it to the repository with:

    git commit -m "Created subfolder for Part2 of of CA3 and a preliminary README.md file"
    git push

Next, we need to [download](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/) the repository that contains an
initial solution of a use case of vagrant from. The use of the provided Vagrantfile configurations shall load the
application into the following links:

    http://localhost:8080/demo-0.0.1-SNAPSHOT/
    http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
    
    http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
    http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
    
### Copy Vagrantfile to repository
To achieve this step, we need to copy the Vagrantfile from the initial solution into ca3-part2.

### Create copy of ca2-part2 project to be used in ca3-part2
Now we need to make a copy of the ca2-part2 project, in order to maintain the original version (i.e. non-Vagrant) in a
stable version. This copy folder was named ca2-part2-v2 in the repository.

### Update Vagrantfile configuration
To attain this step, we modify the following pece of code in the Vagrantfile, linking it to the desired repository.
It should be noted that this repository was made public to avoid credential issues, and inside we will use the folder
ca2-part2-v2.

      # Change the following command to clone your own repository!
      git clone https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git
      cd devops-19-20-b-1090118/ca2-part2-v2/demo
      chmod +x gradlew
      ./gradlew clean build
      sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
      
### Update Spring Application to handle Vagrant
Next, we need to update the project of ca2-part2-v2 to handle Vagrant, following the instructions provided in the
slides. These changes, were added to the bitbucket repository.

### Launch application
With all the previous steps done, now we can opem a `cmd` terminal inside teh ca3-part2 folder, and run the following
command:

    vagrant up
    
This loads the application, and if everything is ok the employee table and its data should now be visible at
http://localhost:8080/demo-0.0.1-SNAPSHOT/.

Note: With the process described, somehow only the table headers (i.e. no contents) are shown in the link.
Weirdly enough, running the same Vagrantfile in a colleagues computer loads the table and its contents. I was unable to
pinpoint the source of the issue to make everything load on my side. Regardless, given that running the same Vagrantfile
in a colleague's machine works, I assume that the implementation of ca3-part2 is correct. 

## Alternative
### VirtualBox versus Hyper-V
The following paragraphs detail a comparison between two virtualization technologies, VirtualBox and Hyper-V. 
Both Hyper-V and VirtualBox are attractive solutions for virtualization, and the choice between them may be difficult.
This comparison is largely based on [this](https://linustechtips.com/main/topic/1017624-hyper-v-vs-virtualbox/)
and [this](https://www.nakivo.com/blog/hyper-v-virtualbox-one-choose-infrastructure/) sources.

VirtualBox:
* Type 2 hypervisor that is sometimes called a hosted hypervisor. A type 2 hypervisor is an application that runs on
the operating system (OS) and is already installed on a host. When a physical computer starts, the operating system
installed on the host loads and takes control. A user starts the hypervisor application (VirtualBox in this case) and
then starts the needed virtual machines. VM hosted processes are created.
* Free, supports a high number of guest and host operating systems, and hence is suitable for multiplatform environments.
* Easiest to setup.
* Requires the fewest amount of resources.
* Easiest way to move, backup or copy a VPS to another machine - simply by copying the single VM file it creates. 

Hyper-V
* Type 1 hypervisor that is also called a bare metal hypervisor. When a physical computer (a host) starts, a Hyper-V
hypervisor takes control from BIOS or UEFI. Then, Hyper-V starts the management operating system, which can be Hyper-V
Server, Windows, or Windows Server. Virtual machines can be started manually by user or automatically, depending on its
settings.
* Hyper-V can only be installed on Windows-based systems. Microsoft virtualization solution would be a good choice for
companies that already use Windows-based environment.
* Advantages regarding stability and speed (because it's built directly into Windows).
* Setting up a Hyper-V VM requires creating a "Virtual Switch" to allow your VM access to your network.
If done incorrectly the setup process can permanently reserve network ports in your machine for VM's, which can create
network problems for your main windows box. Simply removing Hyper-V won't fix this issue in many cases, requiring a 
complete reinstall of Windows itself. 
* Advantages targeted towards enterprise networks and clustered server environments.
* Backing up or moving a Hyper-V vm is incredibly slow and there are many things that can go wrong to break the
process, unless you purchase expensive third-party software.
* Supports clustering features such as failover clustering and load balancing.

### Implementation
In order to use vagrant with Hyper-V, a few changes must be performed. First, on the Windows-based host machine,
Hyper-V must be enabled. This can be performed on a `cmd` terminal with the following command (this will require
restarting the PC to enable Hyper-V):

    DISM /Online /Enable-Feature /All /FeatureName:Microsoft-Hyper-V 
        
In the Vagrantfile configurations, we now need to compatibilize the configuration with the use of an ubuntu-xenial box
that is compatible with Hyper-V (this [link](https://app.vagrantup.com/boxes/search?provider=hyperv&q=ubuntu&utf8=%E2%9C%93)
lists the available Vagrant boxes) with:

    config.vm.box = "generic/ubuntu1604"    # To be used instead of config.vm.box = "envimation/ubuntu-xenial"
    
    db.vm.box = "generic/ubuntu1604"        # To be used instead of db.vm.box = "envimation/ubuntu-xenial"
    
    web.vm.box = "generic/ubuntu1604"       # To be used instead of web.vm.box = "envimation/ubuntu-xenial"

Next, we should also address the way in which RAM is increased, with:

    web.vm.provider "hyperv" do |v|         # To be used instead of web.vm.provider "virtualbox" do |v|

Finally, we must modify the way we call vagrant wo use hyper-v, which can be done by using the following command in
detriment of `vagrant up`:

    vagrant up --provider hyperv
    
This loads the application, and if everything is ok the employee table and its data should now be visible at
http://localhost:8080/demo-0.0.1-SNAPSHOT/.

Note: With the changes proposed here, I was able to boot the config, db and web boxes completely, and the terminal log
shows at the end BUILD SUCCESSFUL. However, nothing is loaded into http://localhost:8080/demo-0.0.1-SNAPSHOT/,
and I could not pinpoint the reason why it fails.

## Closure
At the end of the assignment, we update the repository with the last version of this README file and tag the
repository with:.

    git commit -a -m "Added final version of ca3-part2 README"
    git push
    git tag ca3-part2
    git push origin ca3-part2


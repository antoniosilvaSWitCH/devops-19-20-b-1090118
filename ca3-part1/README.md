# Class Assignment 3 - Part 1

## Goals
Practice with VirtualBox using the same projects from the previous assignments.

## Steps
### Create Virtual Machine (VM)
The first step was to create a VM, following the steps described in the lecture, summarized as follows:
* Minimal installation media of ***Ubuntu 18.04***. 
* **_2048 MB_** RAM
* **_10 GB_** storage
* Network Adapter 1 as ***NAT***
* Network Adapter 2 as ***Host-only Adapter (vboxnet0)***, with IPv4 Address **_192.168.56.1_**

### Setup VM
After creating the VM, the next step was to configure its network, also following the lecture slides:
* Install the network tools with:

        sudo apt install net-tools
    
* Launch network configuration ﬁle with:

        sudo nano /etc/netplan/01-netcfg.yaml

* Edit the file to setup the IP as:
    
        network:
           version: 2
           renderer: networkd
           ethernets:
               enp0s3:
                dhcp4: yes
               enp0s8:
                addresses:
                    - 192.168.56.5/24

* Apply the new changes with:

        sudo netplan apply
        
* Install openssh-server with:

        sudo apt install openssh-server
 
* Launch ssh configuration file with:

        sudo nano /etc/ssh/sshd_conﬁg
        
* Enable password authentication for ssh with by uncomment the line:

        PasswordAuthentication yes

* Restart ssh service with:

        sudo service ssh restart

### Access VM with ssh
Having configured the VM network, it was now possible to access the VM through the host machine with:

    ssh antonio@192.168.56.5

### Install necessary packages in the VM
In order to run the previous assignments in the VM, the installation of various packages is required:

    sudo apt install git
    sudo apt install openjdk-8-jdk-headless
    sudo apt install maven
    sudo apt install gradle
    
### Clone testing repository into the VM
In order to clone the repository into the VM, and having installed git in the previous step, it is now possible to run:

    git clone https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git

### Run ca1
To run ca1, one must execute the following commands. Firstly, one must go to the correct directory with:

    cd devops-19-20-b-1090118/ca1/basic
    
Next, execution permissions must be given to the maven executable in that directory:

    chmod +x mvnw

Next, the application is launched with:

    ./mvnw spring-boot:run
    
No errors were found, which allows opening the application in:

    http://192.168.56.5:8080

This launches the web page with the tabled data and concludes exercise.


### Run ca2-part1
To run ca2-part1, one must execute the following commands. Firstly, one must go to the correct directory with:

    cd devops-19-20-b-1090118/ca2-part1/gradle_basic_demo
    
Next, execution permissions must be given to the gradle executable in that directory:
 
     chmod +x gradlew
 
Next, a build is made with:
 
     ./gradlew build
     
Next, to run the server, the following command was executed:
 
     java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
 
That concludes the operations performed on the VM, and now one must run the client on the host machine. Firstly, the
`build.gradle` file of ca2-part1, namely the task `runClient` was modified to launch the chat in the IP of the VM
(instead of localhost):

	task runClient(...){
		(...)
		args '192.168.56.5', '59001'
	}

Next, in the host machine one must run the client with:

    gradlew runClient

This launches the chat app in the host machine and concludes exercise.

### Run ca2-part2
Regarding ca2-part2, some preliminary issues were addressed prior to the running the application. These pertain to the
`node` and `node_modules` directories that were mistakenly included in the repository. The following commands were
executed in the VM to remove these directories:

    cd devops-19-20-b-1090118/ca2-part2/demo
    sudo rm -rf node
    sudo rm -rf node_modules
    
Next, these directories were also removed from the repository using an IDE in the host machine. this could be made in
the VM terminal, but it was easier to do so in the IDE. The first step was to add the relevant directories to the
.gitignore file:

    /node
    /node_modules
    
Next, commit and push these changes to the repository, after which one must run the next commands (based on the link
provided in Moodle):
    
    git rm -r --cached .
    git add .
    git commit -m ".gitignore fix"
    git push

This should remove the directories permanently in the repository.

Going back to the VM, one must give execution permissions to the gradle executable in the ca2-part2 directory:
                           
       chmod +x gradlew
                           
Next, a build is made with:

    ./gradlew clean build

Next, to run the application, the following command was executed:

    ./gradlew bootRun

No errors were found, which allows opening the application in:

    http://192.168.56.5:8080

This launches the web page with the tabled data and concludes exercise.

### Closure
To finalize, a tag was was given to mark the assignment, and the present README.md was committed and pushed to the
repository: 

    git commit -a -m "Added final version of README.md for ca3-part1"
    git push
    git tag ca3-part1
    git push origin ca3-part1

# Class Assignment 5 - Part 1

## Goals

The goal of this assignment is to practice with Jenkins using the "gradle basic demo" project (ca2-part1).

## Steps
### Getting Started
The first step was to download and run Jenkins via the WAR file following the instructions of:

    https://www.jenkins.io/doc/book/installing/#war-file

Once Jenkins is executed is installed and a Jenkins account is configured, we are now able to our pipeline.

### Configure pipeline with manual script

In order to create a new pipeline, from the Jenkins main page we select ***New Item***, after which we select
***Pipeline***. After naming this pipeline (in my case called `ca5-part1-manual`), we can configure it by defining a
`Pipeline script` that includes:

    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'antoniosilva', url: 'https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd ca2-part1/gradle_basic_demo & gradlew clean assemble'
                }
            }
            stage ('Test'){
                steps {
                 echo 'Testing...'
                 bat 'cd ca2-part1/gradle_basic_demo & gradlew test'
                 junit 'ca2-part1/gradle_basic_demo/build/test-results/test/*.xml'
                }
            }
            stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'ca2-part1/gradle_basic_demo/build/distributions/*'
                }
            }
        }
    }

This script is intended to link to by BitBucket repository, namely to the ca2-part1 project, with stages for `Checkout`, 
`Assemble`, `Test`, and `Archive`. Once the Pipeline is created, we can perform `Build Now` to run the pipeline. If
everything works out, all stages run successfully.

### Configure pipeline with remote script

Alternatively to the approach referred before, one may also configure the pipeline with a Jenkins script file hosted
remotely. After creating a new Pipeline (named `ca5-part1-remote` in my case), we now select the option
`Pipeline script from SCM` using `Git`. We can then link to `Repository URL` using the saved credentials:
    
    https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git
    
The relevant Jenkins file, which includes script reported above, was hosted in my repository in the folder named
ca5-part1. As such the Pipeline must be configured by assigning to `Script Path` the `ca5-part1/Jenkinsfile` path. 
Once the Pipeline is created, we can perform `Build Now` to run the pipeline. If everything works out, all stages run
successfully.

## Closure
At the end of the assignment, we update the repository with the last version of this README file and tag the
repository with:

    git commit -a -m "Added final version of ca5-part1 README"
    git push
    git tag ca5-part1
    git push origin ca5-part1
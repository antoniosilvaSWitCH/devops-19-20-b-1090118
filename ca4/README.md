# Class Assignment 4

## Goals

The goal of this assignment is to use Docker to setup a containerized environment to
execute your version of the gradle version of spring basic tutorial application

## Steps
### Getting Started

The first step was to download the docker-related example from available at:

    https://bitbucket.org/atb/docker-compose-spring-tut-demo/
    
This example was integrated inside the CA4 folder of the repository.

### Modify example DockerFile
Next, we need to edit the `DockerFile` inside the web folder in order to accommodate the ca2-part2-v2 project. These
modifications are intended to link to my own repository. It is also necessary to give execution permissions to gradlew,
and to modify the `.war` filename to reflect the file generated in the build (i.e. `demo-0.0.1-SNAPSHOT.war` instead of
`basic-0.0.1-SNAPSHOT.war`). The modified reads:

    FROM tomcat
    
    RUN apt-get update -y
    
    RUN apt-get install -f
    
    RUN apt-get install git -y
    
    RUN apt-get install nodejs -y
    
    RUN apt-get install npm -y
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://antoniosilvaSWitCH@bitbucket.org/antoniosilvaSWitCH/devops-19-20-b-1090118.git
    
    WORKDIR /tmp/build/devops-19-20-b-1090118/ca2-part2-v2/demo
    
    RUN chmod u+x gradlew
    
    RUN ./gradlew clean build
    
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
    
    EXPOSE 8080
 
### Build and execute

Having concluded the necessary changes to the DockerFile, and having installed Docker in the PC, we must now execute,
inside the folder that has the `docker-compose.yml` file, the following command to perform the build:

    `docker-compose build`

If everything goes smoothly, we execute the docker with:

    `docker-compose up`

In the host we can now open the spring web application using the following URL (note the name of the `.war` file is
consistent with the previous steps):

    http://localhost:8080/demo-0.0.1-SNAPSHOT/

And we can also open the H2 console using the following URL (note the name of the `.war` file is consistent with the
previous steps):

    http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console

For the connection string use:

    jdbc:h2:tcp://192.168.33.11:9092/./jpadb  

### Publish the db and web images to DockerHub
In order to push the images to DockerHub, a number of steps are required, as shown below:
 
1. Log in on https://hub.docker.com/
2. Click on Create Repository.
3. Choose a name (e.g. devops-19-20-b-1090118) and a description for your repository and click Create.
4. Log into the DockerHub from the command line, and enter your credentials when prompted to do so, with:
    `docker login`
5. Check the image ID using 
    `docker images`
6. Tag image you want to push with:
    `docker tag <IMAGEID> ajms/devops-19-20-b-1090118:<TAG>`
7. Push your image to the repository you created:
    `docker push ajms/devops-19-20-b-1090118:<TAG>`
 
 After these steps are carried out, the images are now accessible from DockerHub. For my particular project, they are
 accessible in the repository `ajms/devops-19-20-b-1090118`. The images with tags `ca4` and `ca4_db` refer to the `web`
 and `db` images, respectively. By mistake, the image `web` was tagged simply with `ca4` when the intent was to have
 `ca4_web`, and no straightforward solution was found to correct this mistake.
 
### Commit and push preliminary version of CA4 
After the previous steps, the current version of the CA4 folder containing the modified DockerFile and the current
README.md file was pushed to the repository with:

     git commit -m "Created folder for CA4 with a modified DockerFile and a preliminary README.md file"
     git push
 
### Use a volume of the db container to get a copy of the database file
In order to perform this operation, the following commands were used while the containers are on:

    docker-compose exec db bash
    cp /usr/src/app/jpadb.mv.db /usr/src/data
 
## Alternative
### Kubernetes and Docker
**Disclaimer**
*In previous assignments, the discussion of an alternative was often based on a A versus B comparison, whereby the
advantages and disadvantages of each alternative were discussed. This approach, however, does not apply to the current
assignment, since Kubernetes is not really an alternative to Docker, but rather a technology that complements Docker.
In this context, I chose to make a more thorough discussion surrounding Kubernetes and its use with Docker container
images using existing online sources. The following paragraphs are therefore heavily based on the detailed discussion
provided in `https://www.sumologic.com/blog/kubernetes-vs-docker/`.*

#### Docker and the need for container orchestration
Docker is currently the most popular container platform. When most people talk about Docker they are talking about
Docker Engine, the runtime that allows you to build and run containers. But before you can run a Docker container
they must be built, starting with a Docker File. The Docker File defines everything needed to run the image including
the OS network specifications, and file locations. Now that you have a Docker file, you can build a Docker Image which
is the portable, static component that gets run on the Docker Engine. And if you don’t want to start from scratch
Docker even has a service called Docker Hub, where you can store and share images. 

While Docker provided an open standard for packaging and distributing containerized applications, there arose a new
problem. How would all of these containers be coordinated and scheduled? How do you seamlessly upgrade an application
without any interruption of service? How do you monitor the health of an application, know when something goes wrong
and seamlessly restart it? Solutions for orchestrating containers soon emerged. Kubernetes, Mesos, and Docker Swarm are
some of the more popular options for providing an abstraction to make a cluster of machines behave like one big machine,
which is vital in a large-scale environment.

#### Kubernetes solution
Kubernetes is the container orchestrator that was developed at Google which has been donated to the CNCF and is now open
source. It is a comprehensive system for automating deployment, scheduling and scaling of containerized applications,
and supports many containerization tools such as Docker. Kubernetes is made up many components that do not know are
care about each other. We can break down the components into three main parts.

* The Control Plane - The Master.
The control plane is the orchestrator. Kubernetes is an orchestration platform, and the control plane facilitates that
orchestration. There are multiple components in the control plane that help facilitate that orchestration. Etcd for
storage, the API server for communication between components, the scheduler which decides which nodes pods should run
on, and the controller manager, responsible for checking the current state against the desired state. 

* Nodes - Where pods get scheduled.
Nodes make up the collective compute power of the Kubernetes cluster. This is where containers actually get deployed to
run. Nodes are the physical infrastructure that your application runs on, the server of VMs in your environment. 

* Pods - Holds containers. 
Pods are the lowest level resource in the Kubernetes cluster. A pod is made up of one or more containers, but most
commonly just a single container. When defining your cluster, limits are set for pods which define what resources, CPU
and memory, they need to run. The scheduler uses this definition to decide on which nodes to place the pods.

#### Can you use Docker without Kubernetes?
Docker is commonly used without Kubernetes, in fact this is the norm. While Kubernetes offers many benefits, it is
notoriously complex and there are many scenarios where the overhead of spinning up Kubernetes is unnecessary or
unwanted. In development environments it is common to use Docker without a container orchestrator like Kubernetes.
In production environments often the benefits of using a container orchestrator do not outweigh the cost of added
complexity. Additionally, many public cloud services like AWS, GCP, and Azure provide some orchestration capabilities
making the tradeoff of the added complexity unnecessary. 

#### Can you use Kubernetes without Docker?
As Kubernetes is a container orchestrator, it needs a container runtime in order to orchestrate. Kubernetes is most
commonly used with Docker, but it can also be used with any container runtime. RunC, cri-o, containerd are other
container runtimes that you can deploy with Kubernetes. The Cloud Native Computing Foundation (CNCF) maintains a
listing of endorsed container runtimes on their ecosystem landscape page and Kubernetes documentation provides specific
instructions for getting set up using ContainerD and CRI-O.   
  
### Implementation
**Disclaimer**
*I have spent a few hours trying to implement the alternative (actually a complementary solution using the
docker images created in CA4) with kubernetes. Sadly, I could not risk investing more hours in this and thus hinder the
remaining activities of SWitCH in order to fully implement the alternative. I tried multiple approaches following
countless links, and the end result was always unsuccessful. 
However, I found one source of information that took me (I believe) the furthest in this implementation process. In the
following paragraphs I will describe this (preliminary) implementation solution, and the
final hurdle I ran into that prevents me from concluding this implementation. This solution was based on the steps
provided in https://www.linode.com/docs/kubernetes/deploy-container-image-to-kubernetes/*

Having hosted the Docker images in DockerHub, the solution I adopted starts with the configuration of the Kubernetes
Cluster with `kubectl`. In order to use `kubectl`, it was necessary to download the binary file from 
`https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/windows/amd64/kubectl.exe` and add the folder
where I place the binary to the Windows PATH variables.

The first step was to create a namespace, which is something I believe is entirely optional for this project. In my PC,
I created a directory to store the manifest files with:

    mkdir -p clientx/antonio/
   
The manifest file for your the site’s namespace was created with the following content in a file
`clientx/antonio/ns-antonio-site.yml` with:

    apiVersion: v1
    kind: Namespace
    metadata:
      name: antonio-site

To create the namespace from the `ns-antonio-site.yml` manifest we run:

    kubectl create -f clientx/antonio/ns-antonio-site.yml  
    
Now, to create the service that groups together all pods for the Antonio site, we must first create the respective
manifest file (`clientx/antonio/service-antonio.yml`):

    apiVersion: v1
    kind: Service
    metadata:
      name: antonio-site
      namespace: antonio-site
    spec:
      selector:
        app: antonio-site
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
      type: NodePort

We then create the service for the site:
      
    kubectl create -f clientx/antonio/service-antonio.yml

And we can view the service and its corresponding information:
                
    kubectl get services -n antonio-site
    
Now, to create the deployment, we must create the manifest file (`clientx/antonio/deployment.yml`) with:

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: antonio-site
      namespace: antonio-site
    spec:
      replicas: 3
      selector:
        matchLabels:
          app: antonio-site
      template:
        metadata:
          labels:
            app: antonio-site
        spec:
          containers:
          - name: antonio-site-db
            image: ajms/devops-19-20-b-1090118:ca4_db
            imagePullPolicy: Always
            ports:
            - containerPort: 80
          - name: antonio-site-web
            image: ajms/devops-19-20-b-1090118:ca4
            imagePullPolicy: Always
            ports:
            - containerPort: 80      

We can now create the deployment for the site:
           
    kubectl create -f clientx/antonio/deployment.yml
   
And view the site's deployment with:

    kubectl get deployment antonio-site -n antonio-site
    
After this step, the deployed site was shown as ready in all pods, something that was not achieved in any of the
countless alternatives to this implementation I tried to follow. I therefore believe that I am missing just a few more
details to be able to view the sire with the employee table, since the following commands lead to some issues.

In order to view the site, the first step is to get the worker node’s external IP address. Copy down the EXTERNAL-IP
value for any worker node in the cluster: 

    kubectl get nodes -o wide

The second step is to access the antonio-site services to view its exposed port:

    kubectl get svc -n antonio-site
   
This, however, is where I faced the final hurdle, since the first command results only in a single entry (`docker-desktop`)
with an EXTERNAL-IP of `<none>`. Whilst the port from the second command is shown (`30958`), without the EXTERNAL-IP
there is nothing to access. I further tried (just trying my luck at this point) to access
`http://localhost:30958/demo-0.0.1-SNAPSHOT/` and even the link suggested in the guide
`hhttp://192.0.2.1:30304/demo-0.0.1-SNAPSHOT/`, but with no avail.

## Closure
At the end of the assignment, we update the repository with the last version of this README file and tag the
repository with:.

    git commit -a -m "Added final version of ca4 README"
    git push
    git tag ca4
    git push origin ca4
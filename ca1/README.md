# A. Análise, design e implementação com git

## 1. Tag da versão inicial como v1.2.0
Devem ser utilizados os comandos:

    git tag v1.2.0                  (para atribuir uma tag na versão local)
    git push origin v1.2.0          (push da tag para o repositório)

Nota: Por lapso, a tag dada foi *1.2.0* em vez de *v1.2.0*. Por incapacidade de mudar uma tag existente, este lapso
não foi corrigido.

## 2. Criar uma ramificação email-field
Para criar o novo branch, deve ser utilizado o comando:

    git checkout -b email-field

Este comando cria o novo branch, mudando automaticamente para esse branch.

## 2. Adicionar suporte para o campo de email
Foram feitas várias alterações ao repositório original. Em primeiro lugar, foi adicionado um novo argumento 
no construtor na classe *Employee.java* (String email). Em concomitância, foram adicionadas novas entradas à base de 
dados em *DatabaseLoader.java*, incluindo o novo campo email. Por fim, no ficheiro *app.js*, foi adicionado um novo
campo à classe Employee para ser renderizado o novo campo *employee.email*.

## 4. Adicionar testes unitários para a criação de Employees validando os seus atributos
Foram adicionadas restrições na classe *Employee.java* de forma a não permitir dados inválidos (e.g. null, strings 
em branco, strings só com espaços). Em caso inválido é lançada uma excepção. Mais ainda, foram adicionados testes
unitários de forma a verificar que estes controlos são eficazes.

## 5. Debug das componentes de servidor e cliente
De forma a verificar se a aplicação continua funcional depois das referidas alterações, foi corrido o comando
*mvnw spring-boot_run* no IDE, sendo possível constatar que a tabela de employees é carregada na página
*localhost:8080*.

## 6. Merge das alterações
No contexto de finalização do suporte para email-field, foi corrida a seguinte série de comandos (pressupõe-se a
existência duma issueue #2 referente a esta tarefa):

    git add -A                      (staging)
    git commit -a -m “fix #2”       (commit)
    git push                        (push)
    git checkout master             (para alternar para o branch master)
    git merge email-field           (para fazer merge do branch master com o branch alternativo email-field)
    git tag v1.3.0                  (para atribuir uma tag na versão local)
    git push origin v1.3.0          (push da tag para o repositório)

## 7. Criar uma ramificação fix-invalid-email

Para criar o novo branch, deve ser utilizado o comando:

    git checkout -b fix-invalid-email

Uma vez criado o novo branch, foi adicionado à classe *Employee.java* um novo método *isEmailValid* que controla
se a string contém o símbolo *@*. Este comtrolo é adicional ao já existente nesta fase, nomeadamente de o argumento
email é válido (i.e. não null, vazio, só espaços).

De forma a verificar se a aplicação continua funcional depois das referidas alterações, foi corrido o comando
*mvnw spring-boot_run* no IDE, sendo possível constatar que a tabela de employees é carregada na página
*localhost:8080*.

Uma vez concluído este processo, os passos são idênticos aos referidos na Secção 6 (pressupõe-se a existência duma
issue #3 referente a esta tarefa):

    git add -A                      (staging)
    git commit -a -m “fix #3”       (commit)
    git push*                       (push)
    git checkout master             (para alternar para o branch master)
    git merge fix-invalid-email     (para fazer merge do branch master com o branch alternativo fix-invalid-email)
    git tag v1.3.1                  (para atribuir uma tag na versão local)
    git push origin v1.3.1          (push da tag para o repositório)

## 8.Tag do repositório com ca1
Aquando da conclusão do presente relatório, os seguintes passos foram tomados 
(pressupõe-se a existência duma issue #4 referente a esta tarefa):

    git add -A                      (staging, onde apenas o ficheiro Readme.md sofreu alterações)
    git commit -a -m “fix #4”       (commit)
    git push                        (push)
    git tag ca1                     (para atribuir uma tag na versão local)
    git push origin ca1             (push da tag para o repositório)

***

# B. Análise e design de uma alternativa a git

## 1. Introdução
No contexto da pesquisa sobre uma alternativa ao Git, várias opções encontram-se disponíveis. Estas opções podem ser
agrupadas em duas arquiteturas de sistema principais, nomeadamente:

- **Centralized Version Control Systems (CVCS)** 
    - [Subversion](https://subversion.apache.org/)
    - [Concurrent Versions System](http://www.nongnu.org/cvs/)
    - [Perforce](https://www.perforce.com/)
    - (...)
- **Distributed Version Control Systems (DVCS)** 
    - [Git](https://git-scm.com/)
    - [Mercurial](https://www.mercurial-scm.org/)
    - (...)
    
Após alguma pesquisa das alternativas acima referidas, foi dada preferência ao Perforce para a realização do presente
trabalho. Esta opção foi maioritariamente baseada no facto de serem encontradas fontes sólidas de comparação com Git,
o que simplifica o processo de interiorização e comparação. As referências usadas neste contexto encontram-se incluídas
no fim do presente relatório.

É importante referir que ambas as alternativas (i.e. Git ou Perforce) são opções viáveis a ter em consideração. Por
exemplo, Git funcionará de forma bastante eficaz com projetos baseados em código com equipas de desenvolvimento não
muito extensas e com ciclos rápidos de desenvolvimento e lançamento (e.g. desenvolvimento *web*). Por outro lado, para
projetos com a complexidade de incorporação de ativos digitais (e.g. desenvolvimento de jogos), o uso de Perforce pode
ser mais vantajoso, uma vez que Git nativo armazena todas as versões passadas de um dado ficheiro.

## 2. Perforce vs. Git
### 2.1. Modelos centralizado e distribuído 
Com o modelo Git distribuído, é feito o download do código-fonte, juntamente com um histórico completo da versão,
para um ambiente local onde as alterações podem ser efetuadas. No entanto, uma equipa de desenvolvedores, cada um com
uma própria cópia do repositório, precisa de coordenar o compartilhamento de alterações. Esta realidade levanta portanto
a questão: qual destas versões do repositório é a *master*? É por esta razão que a adoção do modelo centralizado de Git
tem ganho tração, uma vez que as alterações podem ser submetidas sob pedidos *pull* ou *merge* a um ramo de
desenvolvimento *master* alojado num servidor Git dedicado.  

Por outro lado, o Perforce (por via da tecnologia Helix Core), basei-se nativamente num modelo centralizado. Toda a
informação é alojada num único local, garantindo que toda a quipa possui sempre a última versão. Mais ainda, as
modificações efetuadas são alvo de *commit* para um servidor central que aloja a única fonte da verdade. Este aspeto
difere do modelo Git no sentido em que as alterações não ficam alojadas só (inicialmente) no repositório local. 

### 2.2. Ramificações (branching)
Num ambiente Git, quando um *branch* é criado, é possível trabalhar na ramificação local de forma imediata. Após as
modifiações ao código seram concluídas, é então possível fazer *commit*, havendo a opção de *merge* (i.e. unificar
mantendo o historial de alterações) ou *rebase* (i.e. unificar discartando o historial de alterações). No entanto, um
*merge* da versão local com o ramo *master* difere do *push* das alterações ao repositório central. No caso de equipas
extensas trabalhando nos mesmos ficheiros, conflitos de *merge* podem ocorrer, sendo vital ter a última versão central
(i.e. *pull*) andtes de qualquer *merge*. Para equipas extensas, este processo de atualização manual pode ser
extremamente moroso, sendo também relevante referir que nestas circunstâncias, a resolução dos referidos conflitos
pode vir a ser bastante complexa.  

No Helix Core, as ramificações são feitas no nível da hierarquia de arquivos. Os membros da equipe podem escolher
arquivos específicos para efetuar o *checkout* e enviar de volta ao repositório central. Checkouts exclusivos 
proporcionam visibilidade directa de onde os outros colaboradoes estão a trabalhar. E com permissões granulares até o
nível do arquivo, os administradores podem manter os arquivos mais importantes protegidos. Os chamados *Perforce
Streams*, i.e. a forma como se lida com ramificações (*branch*) e unificações (*merge*), simplifica a configuração do
espaço de trabalho. Os desenvolvedores podem alternar facilmente entre *streams* (*branches*) e é fácil ver como as
alterações são propagadas. Assim como no Git, ao enviar alterações no *stream*/*branch* mestre, os conflitos podem
ocorrer, sendo de resolução mais fácil.

## 3. Design de solução
Na tabela abaixo, apresenta-se um sumário comparativo entre os comandos disponíveis num ambiente Git e em Perforce. Este
resumo inclui os comandos usados na implementação Git do exercício CA1. 

| Git        | Usage           | Perforce  |
| ------------- |:-------------:| -----:|
| `git config`     | Set a variety of parameters for the version control system from the client | `P4CONFIG` |
| `git init`     | Create a new repository | `p4 init` |
| `git clone`     | Setup and begin work on a project from a server on your workstation | `p4 sync` |
| `git fetch`     | Get files/changes from a remote server onto your workstation | `p4 sync` |
| `git add`     | Add a new file | `p4 add`,`p4 edit` |
| `git commit`     | Record changes to the repository | `p4 submit` |
| `git reset`     | Reset your working directory to your last commit | `p4 clean` |
| `git log`     | Show a list of commits on a branch | `p4 changes` |
| `git rm`     | Remove a file | `p4 delete` |
| `git status`     | View all files that need to be committed | `p4 status` |
| `git tag`     | Tag/label a specific revision | `p4 tag` |
| `git remote`     | Manage your connections to repos | `p4 remote` |
| `git checkout`     | Work in a branch | `p4 switch -c` |
| `git branch`     | Get a list of all branches, create new local branches, and delete feature branches | `p4 branches` |
| `git push`     | Push changes to a remote server | `p4 push` |
| `git pull`     | Get files from a remote server | `p4 pull` |
| `git merge`     | Merge changes between branches | `p4 merge`, `p4 integrate` |
| `git rebase`     | Merge and squash down your history | Helix Core preserves your history |
| `git stash`     | Temporarily store modified files locally | `p4 shelve` (stores modified files on the server) |

Como é possível constatar pela tabela acima exposta, existe uma consistência global entre os comandos disponíveis em Git
e Perforce. Neste contexto, a implementação da solução do repoitório do exercício CA1 em Perforce devera ocorrer de
forma natural. Os passos e comandos usados serão globalmente similares, sendo esperadas diferenças apenas ao nível da
criação de um novo *branch*. Neste caso, e como foi descrito anteriormente, supõe-se que num ambiente Perforce seria
possível criar um *stream* com referência apenas aos ficheiros a modificar (*Employee.java*, *DatabaseLoader.java*,
*app.js*), sendo as modificações feitas enviadas de volta ao repositório central como em Git (i.e. usando os comandos 
*add*, *commit*, *push*, *checkout*, *merge*, *tag*). É importante referir, no entanto, que Não foi efetuada nenhuma
implementação da solução do repositório CA1 em Perforce, pelo que não é possível descrever dificuldades decorrentes
dessa implementação. 

## Referências

*https://www.atlassian.com/blog/software-teams/version-control-centralized-dvcs*

*https://www.perforce.com/blog/vcs/git-vs-perforce-how-choose-and-when-use-both*

*https://www.perforce.com/blog/vcs/perforce-vs-git-commands-you-should-know*

*https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different*

